'''
Created on 2013-01-12
@author: Daria
'''
import weka_params

def load_all_stats_from_tsv(users, exlclusion, data_file):
    """
    params:
        users, exlclusion, data_file
    returns:
        stats,userlist, feature_list
    """
    stats= []
    userlist= []
    file=open(data_file ,"r")
    feature_list = file.readline().rstrip('\n').split('\t')
    feature_list.remove(feature_list[0])
    i=0
    while i < users:
        line = file.readline()
        s = line.split('\t')
        print i,">>",s[0]
        ss=[]
        if i in exlclusion:
            print "ignore:",s[0]
            i+=1
            continue
        userlist.append(s[0])
        s.remove(s[0])
        for x in s:
#            print "'",x,"'"
            if x=='' or x.isspace():
#                print "<",0,">"
                ss.append(0.0)
            elif x=='#DIV/0!':
                ss.append(0.0)
            elif x=='NaN':
                ss.append(0.0)
            elif x in ["Control", "Feedback", "main"]:
                ss.append(0.0)
            else:
                ss.append(float(x))
        i+=1
        stats.append(ss)
#        print s
    
    return stats,userlist, feature_list

def load_user_stats_from_tsv(filename, first_Col_is_label = True):
    """
    Daria: this function is modified from original to deal with not calculated features (dynamic AOIs functionality)
    
    If AOI hasn't appeared on screen the corresponding features won't be calculated. To avoid this the feature values that are empty will be filled with -1
    
    featurelabels is a predefined list of features. The user features should be reordered according positions of features in this list. 
    params:
        filename, first_Col_is_label = True
        featurelabels
    returns:
        stats, sectionlist
    """
    stats=[]
    sectionlist=[]
    file = open(filename ,"r")
    featline = file.readline()
    """ create a list of features from file """
    features = featline[:-1].split('\t')
    featnum = len(features)
    featurelabels = features
    print "Number of features is: " + str(featnum)
    feature_ind = [] 
    if first_Col_is_label:
        for el in featurelabels:
            feature_ind.append(features.index(el)-1)
    else:
        for el in featurelabels:
            feature_ind.append(features.index(el))
    i=0
    print "Full Set Of Features: " + str(featurelabels)
    print "Online Set Of Features: " + str(features)
    print "indexes: " + str(feature_ind)
    
    for line in file.readlines():
        s=line.rstrip('\n').split('\t')
        while len(s)< featnum:
            s.append('-1')
        ss=[]
        if first_Col_is_label:
            sectionlist.append(s[0])
            s.remove(s[0])
        else:
            sectionlist.append(str(i))           
        #for x in s:
        for ind in feature_ind:
#            print "'",x,"'"
            x = s[ind]
            if x=='' or x.isspace():
#                print "<",0,">"
                ss.append(0.0)
            elif x=='#DIV/0!':
                ss.append(0.0)
            elif x in ["Control", "Feedback", "main"]:
                ss.append(0.0)
                
            elif x.find("MT208")>-1: #if USER id is one of the features 
                ss.append(x)
                
            else:
                ss.append(float(x))
        i+=1
        stats.append(ss)
#        print s
    
    return stats, sectionlist, featurelabels


def embed_in_arff_featnames_label(featnames, data, relation_name = "online_learning", class_labels = weka_params.CLASS_LABELS):
    '''
    PURPOSE:
        Create arff file from given data (containing values for featnames).  
    
    INPUTS:
        featnames = list of feature names to be added to the arff (important for header part) 
        data = list with actual data values
        relation_name = name of relation to be created 
        class_labels = labels for classes in the model. By default value from weka_params used
    OUTPUT:
        list of lines to be written to arff file
    '''
    header = []
    header.append("@relation '" + relation_name + "'\n\n")
    
    """ add attribute names to arff header"""
    for act in featnames:
        header.append("@attribute '" + act + "' numeric\n")

    """ add class attribute to the header"""
    
    line = "@attribute class {"
    for key in class_labels.keys():
        line += class_labels[key] + ", "
    
    header.append(line[:-2]+ "}\n") #append line excluding final comma and space
    header.append('@data\n\n')
    body = []
#    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>", shape(data)[0]
    for i in xrange(len(data)):
        body.append((str(data[i]).strip('[')).rstrip(']')+"\n")
        
    result = []
    result.extend(header)
    result.extend(body)
    
    return result

