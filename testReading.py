'''
Created on Sep 18, 2013

@author: bondaria
'''

import runModels_repeatedCV
import weka_params


DIR =  "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\McGill Project Data - online\\OnlineFeatures\\"
INPUT =  "trainingset.tsv"
TRAINDIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\McGill Project Data - online\\_for_testing_only\\" 
TESTDIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\McGill Project Data - online\\_for_testing_only\\"
testfold = ["MT208PN41005", "MT208PN41049", "MT208PN41017", "MT208PN41023", "MT208PN41088"]
featurelabels = weka_params.feat_NB_Gaze_labels
trainFileName = "training.arff"
testFileName = "testing.arff"

runModels_repeatedCV.CreateTrainFromTsv(DIR, INPUT, testfold, featurelabels, trainFileName, DIRTRAIN = TRAINDIR, list_of_participants = weka_params.ul, classlist = weka_params.CLASS, classlabels = weka_params.CLASS_LABELS)
userintervals = runModels_repeatedCV.CreateTestFromTSVs(DIR, testfold, featurelabels, testFileName, input_prefix = weka_params.USERDATAFILE_PREFIX, DIRTEST = TESTDIR, list_of_participants = weka_params.ul, classlist = weka_params.CLASS, classlabels = weka_params.CLASS_LABELS)
print userintervals
