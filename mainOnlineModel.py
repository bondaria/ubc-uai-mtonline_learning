'''
Created on Aug 2, 2013

@author: bondaria
'''

NRUNS = 10#number of run in repeated CV
NFOLDS = 10
SUFFIX = "_seed_8_shuffled"

from runModels_repeatedCV import *
from datetime import datetime
import weka_params
#ubc
DIR = "C:\\Users\\bondaria\\Documents\\UAI\\temp\\input\\"
OUTDIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\McGill Project Data - online\\_for_testing_only\\"


#Classifiers_feat = {"Naive_Bayes_RepCV": [[weka_params.feat_NB_Gaze_labels], ["online_repCV_NB_Gaze.tsv"]]}

Classifiers_feat = {"Multilayer_Perceptron_RepCV": [[weka_params.feat_MLP_Actions_labels, "online_repCV8_MLP_Actions.tsv"], [weka_params.feat_MLP_Full_labels, "online_repCV8_MLP_Full.tsv"]],
                    "Simple_Logistic_Regression_RepCV": [[weka_params.feat_SLR_Gaze_labels, "online_repCV8_SLR_Gaze.tsv"], [weka_params.feat_SLR_Full_labels_final, "online_repCV8_SLR_Full.tsv"]],
                    "Naive_Bayes_RepCV": [[weka_params.feat_NB_Gaze_labels, "online_repCV8_NB_Gaze.tsv"], [weka_params.feat_NB_Full_labels, "online_repCV8_NB_Full.tsv"]],
                    "SMO_RepCV": [[weka_params.feat_SMO_Full_labels, "online_repCV8_SMO_Full.tsv"],[weka_params.feat_SMO_Gaze_labels, "online_repCV8_SMO_Gaze.tsv"]]
                  }
LOG = "LOG" + SUFFIX + ".txt"
log = open(OUTDIR+LOG, "w")
METHODS = ["Simple_Logistic_Regression_RepCV", "Naive_Bayes_RepCV", "Multilayer_Perceptron_RepCV", "SMO_RepCV"] #Classifiers.keys()
METHODS = ["SMO_RepCV"]
lines = []
line = "section\tMETHOD Name\tFile\tOverall Acc\tLL Accuracy \t HL accuracy \t kappa\n"
lines.append(line)


CVSamples = CreateSamplesStratifiedFromUserList(10,10) #create folds for 10 run 10 fold cross validation
#create folds for Leave One Out Cross Validation
# CVSamples = []
# el = []
# for i in xrange(50):
#     el.append([i])
# CVSamples.append(el)
print CVSamples

""" basic online classifiers """
for METHOD in METHODS:
    for model in Classifiers_feat[METHOD]:
        """ build models and output results """
        print "Working on Classifier "+ METHOD
        log.write("METHOD: " +  METHOD)
         
        dict_Classifiers = {}
        featureset = model[0]
        dict_Classifiers[METHOD] = featureset
        log.write("Featureset: " + str(featureset[1:-1]))
        start = datetime.now()
        final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa = runClassifierWithRepeatedCV(DIR, dict_Classifiers, NRUNS, NFOLDS, CVSamples)
        end = datetime.now()
        diff = end - start
        duration = str(diff.seconds)+str(".")+str(diff.microseconds / 1000)
        print "INPUT: "+"ONLINE"+". \nCalculations took: " + duration
        log.write("INPUT: "+"ONLINE"+". \nCalculations took: " + duration)
         
        """ output results"""
        lines = []
        line = "section\tMETHOD Name\tFile\tOverall Acc\tLL Accuracy \t HL accuracy \t kappa\n"
        lines.append(line)
        for i in xrange(100):
            line = str(i) + "\t" + METHOD + "\t"  + "ONLINE" + "\t"+str(final_acc[i]) +"\t"+ str(final_LL_acc[i]) + "\t" +str(final_HL_acc[i]) + "\t" + str(final_kappa[i])+"\n"
            lines.append(line)
        input = OUTDIR + model[1]
        with open(input, 'w') as f:
            for line in lines:
                #print line
                f.write(line)
print "calculating ensemble ... "

""" Online Ensemble Model """
# 
# log.write("Ensemble modeling")
# 
# Classifiers_3Best_1 =  {"Multilayer_Perceptron_RepCV" : [weka_params.feat_MLP_Full_labels],
#                         "Simple_Logistic_Regression_RepCV" : [weka_params.feat_SLR_Gaze_labels, weka_params.feat_SLR_Full_labels_final]
#                    
#                    }
# 
# "full featureset only"
# Classifiers_FULL = {"Simple_Logistic_Regression_RepCV" : [weka_params.feat_SLR_Full_labels_final], 
#                     "Multilayer_Perceptron_RepCV" : [weka_params.feat_MLP_Full_labels],
#                     "Naive_Bayes_RepCV" : [weka_params.feat_NB_Full_labels]}
# 
# 
# start = datetime.now()
# final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa = runClassifierWithRepeatedCV(DIR, Classifiers_3Best_1, NRUNS, NFOLDS, CVSamples,  Ensemble = True)
# end = datetime.now()
# diff = end - start
# duration = str(diff.seconds)+str(".")+str(diff.microseconds / 1000)
# print "INPUT: "+"ONLINE"+". \nCalculations took: " + duration
# log.write("INPUT: "+"ONLINE"+". \nCalculations took: " + duration)
# 
# """ output results"""
# lines = []
# line = "section\tMETHOD Name\tFile\tOverall Acc\tLL Accuracy \t HL accuracy \t kappa\n"
# lines.append(line)
# for i in xrange(100):
#     line = str(i) + "\t" + "Ensemble" + "\t"  + "ONLINE" + "\t"+str(final_acc[i]) +"\t"+ str(final_LL_acc[i]) + "\t" +str(final_HL_acc[i]) + "\t" + str(final_kappa[i])+"\n"
#     lines.append(line)
# input = OUTDIR + "Ensemble"+ SUFFIX+".tsv"
# with open(input, 'w') as f:
#     for line in lines:
#         #print line
#         f.write(line)


log.close()