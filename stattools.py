# -*- coding:utf-8 -*-
"""
Created on 2010-01-21

@author: Samad

updated by bondaria
June 2013
"""
import numpy as np
from numpy import array,empty,shape,zeros

from scipy import stats
def zscore(arr):
    arr=array(arr)
    res=empty(shape(arr),float)
    for i in xrange(shape(arr)[1]):
        if(arr[:,i].min()==arr[:,i].max()):
            col=zeros((shape(arr)[0]))
        else:
            col=(arr[:,i]-np.mean(arr[:,i]))/np.std(arr[:,i],ddof=1)
        res[:,i]=col
    return res

def zscore2(arr):
    arr=array(arr)
    res=empty(shape(arr),float)
    for i in xrange(shape(arr)[1]):
        rangea=[arr[:,i].min(),arr[:,i].max()]
        if(rangea[0]==rangea[1]):
            col=zeros((shape(arr)[0]))
        else:
            col=stats.zmap(arr[:,i],arr[:,i])
        res[:,i]=col
    return res

def squared_error(a,b):
    return sum((array(a)-array(b))**2) 

def distance(a,b):
    return np.sqrt(sum((array(a)-array(b))**2))


def euclidian_distance(a,b):
    return np.sqrt(sum((array(a)-array(b))**2))

def squared_distance(a,b):
    return sum((array(a)-array(b))**2)

def distance2d(a,b):
    return np.sqrt((a[0]-b[0])**2+(a[1]-b[1])**2)

def welsh_t(a,b, axis=0):
    v1=np.var(a, axis,ddof=1)
    v2=np.var(b, axis,ddof=1)
    N1=shape(a)[axis]
    N2=shape(b)[axis]
    ts= (np.mean(a, axis)-np.mean(b, axis))/np.sqrt(v1/N1+v2/N2)
    
    v1=array(v1)
    v2=array(v2)
    vs=(v1/N1+v2/N2)**2/(v1**2/(N1**2*(N1-1))+v2**2/(N2**2*(N2-1)))
    ps=stats.t.sf(abs(ts),vs)
    return ts,ps,vs

def cohen_d(a,b,axis):
    v1=np.var(a, axis,ddof=1)
    v2=np.var(b, axis,ddof=1)
    N1=shape(a)[axis]
    N2=shape(b)[axis]
    d=(np.mean(a, axis)-np.mean(b, axis))/np.sqrt(((N1-1)*v1+(N2-1)*v2)*1.0/(N1+N2))
    return d

def anova_oneway(a,b,c,axis=0):
    aa=array(a)
    bb=array(b)
    cc=array(c)
    fs=[]
    ps=[]
    if axis==0:
        f,p = stats.f_oneway(aa,bb,cc)
        fs.append(f)
        ps.append(p)
    else:
        for i in range (shape(a)[axis]):
            f,p = stats.f_oneway(aa[:,i],bb[:,i],cc[:,i])
            fs.append(f)
            ps.append(p)
    return fs,ps

def anova_oneway_list(list,axis=0):
    alist=[]
    for l in list:
        alist.append(array(l))
    fs=[]
    ps=[]
    if axis==0:
        f,p = stats.f_oneway(*alist)
        fs.append(f)
        ps.append(p)
    else:
        for i in range (shape(list[0])[axis]):
            templist = []
            for l in alist:
                templist.append(l[:,i])
            f,p = stats.f_oneway(*templist)
            fs.append(f)
            ps.append(p)
    return fs,ps

def kappa(TP, FP, TN, FN):
    agreements = TP + TN
    total = TP + FP + TN + FN
    ef_P = (TP + FP) * (TP + FN)
    ef_N = (TN + FN) * (TN + FP)
    ef = (float(ef_P + ef_N))/total
    kappa = (float(agreements - ef))/(total - ef)
    return kappa
def precision(TP, FP, TN, FN):
    return TP/(TP+FP)
def recall(TP, FP, TN, FN):
    return (float(TP))/(TP+FN)
def accuracy (corr, incorr):
    #print "sum: ", corr, "+", incorr
    return (float(corr))/(corr + incorr)

def processOneRun(TLL, FLL, THL, FHL):
    acc_overall = accuracy(TLL+THL, FLL + FHL)
    acc_LL = accuracy(TLL, FHL)
    acc_HL = accuracy(THL, FLL)
    kappa_overall = kappa(TLL, FLL, THL, FHL)
#     print "Run Accuracy: ", acc_overall
#     print "Low Learners accuracy ", acc_LL
#     print "High Leaners accuracy ", acc_HL
#     print "Kappa: ", kappa_overall
    return acc_overall, acc_LL, acc_HL, kappa_overall 

def FindPredictedFromFile(DIR, inputfile):
    predicted = []
    resfile=open(DIR + inputfile)
    for line in resfile.readlines():
        if line.count(":")==2:
            c = line.split(":")
            predicted.append(int(c[1][-1])-1)
    resfile.close()
    return predicted
    
def CalculateStatsFromFile(DIR, inputfile):
    actual = []
    n_USERS = 0
    predicted = []
    resfile=open(DIR + inputfile) 
    for line in resfile.readlines():
        if line.count(":")==2:
            c = line.split(":")
            n_USERS += 1
            actual.append(int(c[0][-1]))
            predicted.append(int(c[1][-1]))
            #print [int(c[0][-1]),int(c[1][-1])]
    #print actual
    #print predicted
    return CalulcateStatsFromLists(actual, predicted, n_USERS)
def ClaculateStatsFromMappedDict(class_labels, dict_predicted, userlist):
    """
    PURPOSE:
    
    INPUT:
    
    OUTPUT:
    
    """
    TLL = []
    FLL = []
    THL = []
    FHL = []
    for i in xrange(100):
        TLL.append(0)
        FLL.append(0)
        THL.append(0)
        FHL.append(0)
        for pid in userlist:
            if class_labels[userlist.index(pid)] == dict_predicted[pid][i]:
                if class_labels[userlist.index(pid)] == 0:
                    TLL[i] += 1
                elif class_labels[userlist.index(pid)] == 1:
                    THL[i] += 1
                else:
                    raise RuntimeError("Extra Class Label detected when calculating stats. Check code!")
            else:
                if class_labels[userlist.index(pid)] == 0:
                    FHL[i] += 1
                elif class_labels[userlist.index(pid)] == 1:
                    FLL[i] += 1
                else:
                    raise RuntimeError("Extra Class Label detected when calculating stats. Check code!")
    return TLL, FLL, THL, FHL
                
                 
        
    
def CalulcateStatsFromLists(actual, predicted, USERS):
    TLL = 0
    FLL = 0
    THL = 0
    FHL = 0
    for i in xrange(USERS):
        #print i
        #print actualValue
        #print predictedValue
        #print actual[i], predicted[i]
        if actual[i] == predicted[i]:
            if actual[i] == 1:
                TLL += 1
            elif actual[i] == 2:
                THL += 1
            else:
                raise RuntimeError("Extra Class Label detected when calculating stats. Check code!")
        else:
            if actual[i] == 1:
                FHL += 1
            elif actual[i] == 2:
                FLL += 1
            else:
                raise RuntimeError("Extra Class Label detected when calculating stats. Check code!")
        
    return TLL, FLL, THL, FHL
