'''
This module is created to calculate performance of classifiers online
Classifiers ti be supported:
- Simple Logistic Regression on Gaze feature set
-Simple Logistic Regression on Full
- Multilayer Perceptron on Actions
- Multilayer Perceptron on Full 

10 run 10 fold cross validation will be supported (Leave One Out Cross Validation will be implemented for now)

@author: bondaria
'''

from MTLogParser_for_Online.MTLogAnalyzer import MTLogAnalyzer
from MTLogParser_for_Online.Log import Logger
from MTLogParser_for_Online.Events import *
from MTLogParser_for_Online.Utils import *



LOGDIR =  "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\McGill Project Data - online\\LOGS-12-50p\\"
#LOGDIR =  "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\McGill Project Data - online\\LOGS\\"

OUTDIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\McGill Project Data - online\\OnlineActions\\"
stoptime_increment = 300000 #5 minutes
stoptime_increment = 120000 #2 minutes
stopt = 0
#STOPTIMES = []
#for i in xrange(36):
#    STOPTIMES.append((i+1)*stoptime_increment)
    
#print STOPTIMES
""" List_of_participants - is list of participants in current study (total for thesis 50p """
list_of_participants = ['MT208PN41005', 'MT208PN41008', 'MT208PN41009', 'MT208PN41010', 'MT208PN41011', 
                         'MT208PN41012', 'MT208PN41013', 'MT208PN41017', 'MT208PN41018', 'MT208PN41019', 
                         'MT208PN41020', 'MT208PN41023', 'MT208PN41024', 'MT208PN41027', 'MT208PN41029', 
                         'MT208PN41030', 'MT208PN41033', 'MT208PN41035', 'MT208PN41037', 'MT208PN41038', 
                         'MT208PN41039', 'MT208PN41040', 'MT208PN41041', 'MT208PN41042', 'MT208PN41043', 
                         'MT208PN41045', 'MT208PN41049', 'MT208PN41050', 'MT208PN41051', 'MT208PN41052', 
                         'MT208PN41053', 'MT208PN41054', 'MT208PN41058', 'MT208PN41059', 'MT208PN41060', 
                         'MT208PN41061', 'MT208PN41064', 'MT208PN41068', 'MT208PN41070', 'MT208PN41072', 
                         'MT208PN41076', 'MT208PN41080', 'MT208PN41081', 'MT208PN41082', 'MT208PN41084', 
                         'MT208PN41085', 'MT208PN41086', 'MT208PN41087', 'MT208PN41088', 'MT208PN41091']
#list_of_participants.remove("MT208PN41012")
#list_of_participants = ['MT208PN41005']
CUTOFFS = []
#create dictionary of features for all participants:
list_of_outputs = [] #list of outputfiles
OnlineActionFeatures = {}
for part in list_of_participants:
    OnlineActionFeatures[part] = []

ListOfFeatrues = "ActionsFeaturesNoCorrel10minAver"
"""
Step 0 - define number of sections for each participant
"""



"""
STEP 1 - Calculate Action-related features

Thus implementation is based on loganalyzer.py provided by Francois
"""
npart = len(list_of_participants) #number of participants 
# mtlogspath - path to day 1-2 logs 
mtlogspath = LOGDIR
logger = Logger("info").logger
while npart > 0:
    stopt +=  stoptime_increment
    logger.info("Current CUTOFF is: " + str(stopt))
    CUTOFFS.append(stopt)
    mtla = MTLogAnalyzer(logger, False, False, False, mtlogspath, StopTimeStamp = stopt)
    rows = []
    npart = len(mtla.subjects)
    for i, subj in enumerate(mtla.subjects):
        logger.info("Treating subject " + str(subj.ID) + "...")
        
        mtla.subjects[i].computeExtraAttributes(logger, mtla.nonValidTestQuestions, mtla.nbSubgoalsSetInitially, mtla.nbPagesInSystem, mtla.matTestPageDict, mtla.matPageSubgoalDict, mtla.minNoteTime, StopTimeStamp = stopt)
        #print "Printing features for UBC Actions Features (no correlation)"
        if rows == []:
            rows = [subj.getSummaryRowList(logger,["ID", ListOfFeatrues], [], True)]
            with open(OUTDIR + "listoffeaturesNoCorrel.txt","w") as f:
                for el in rows[0]:
                    f.write(el +"\n")
        if (OnlineActionFeatures[subj.ID] == []):
            """add line with feature names"""
            OnlineActionFeatures[subj.ID].append(subj.getSummaryRowList(logger,["ID", ListOfFeatrues], [], True))
        rows.append(subj.getSummaryRowList(logger,["ID", ListOfFeatrues], [], False))
        OnlineActionFeatures[subj.ID].append(subj.getSummaryRowList(logger,["ID", ListOfFeatrues], [], False))
        #Utils.exportList2Excel(rows, DIR + ListOfFeatrues + str(stopt) + ".csv")
mtla = MTLogAnalyzer(logger, False, False, False, mtlogspath, StopTimeStamp = None)
rows = []
for i, subj in enumerate(mtla.subjects):
    logger.info("Treating subject " + str(subj.ID) + "...")
    mtla.subjects[i].computeExtraAttributes(logger, mtla.nonValidTestQuestions, mtla.nbSubgoalsSetInitially, mtla.nbPagesInSystem, mtla.matTestPageDict, mtla.matPageSubgoalDict, mtla.minNoteTime, StopTimeStamp = stopt)
    #print "Printing features for UBC Actions Features (no correlation)"
    if (OnlineActionFeatures[subj.ID] == []):
        """add line with feature names"""
        OnlineActionFeatures[subj.ID].append(subj.getSummaryRowList(logger,["ID", ListOfFeatrues], [], True))
    rows.append(subj.getSummaryRowList(logger,["ID", ListOfFeatrues], [], False))
    OnlineActionFeatures[subj.ID].append(subj.getSummaryRowList(logger,["ID", ListOfFeatrues], [], False))
    #Utils.exportList2Excel(rows, DIR + ListOfFeatrues + str(stopt) + ".csv")

for part in list_of_participants:
    Utils.exportList2Excel(OnlineActionFeatures[part], OUTDIR + "ONL_ACT_FEAT_"+part+".csv")
    list_of_outputs.append("ONL_ACT_FEAT_"+part+".csv")


logger.info(str(CUTOFFS))
logger.info("Action features calculated normally finished normally")


"""
STEP 2 - Calculate Gaze Features

"""


"""
STEP 3 - Sync Gaze and Actions Features

Note: There is an offset between Gaze and Actions

"""

"""
STEP 4
Create ARFF files
"""


"""
STEP 5 - Perform 10 run 10 fold CV (implemented for Ensemble modeling)

"""




# for stopt in STOPTIMES:
#     mtla = MTLogAnalyzer(logger, False, False, False, mtlogspath, StopTimeStamp = stopt)
#     rows = []
#     for i, subj in enumerate(mtla.subjects):
#         logger.info("Treating subject " + str(subj.ID) + "...")
#         mtla.subjects[i].computeExtraAttributes(logger, mtla.nonValidTestQuestions, mtla.nbSubgoalsSetInitially, mtla.nbPagesInSystem, mtla.matTestPageDict, mtla.matPageSubgoalDict, mtla.minNoteTime, StopTimeStamp = stopt)
#         #print "Printing features for UBC Actions Features (no correlation)"
#         if rows == []:
#             rows = [subj.getSummaryRowList(logger,["ID", ListOfFeatrues], [], True)]
#             with open(DIR + "listoffeaturesNoCorrel.txt","w") as f:
#                 for el in rows[0]:
#                     f.write(el +"\n")
#         if (OnlineActionFeatures[subj.ID] == []):
#             """add line with feature names"""
#             OnlineActionFeatures[subj.ID].append(subj.getSummaryRowList(logger,["ID", ListOfFeatrues], [], True))
#         rows.append(subj.getSummaryRowList(logger,["ID", ListOfFeatrues], [], False))
#         OnlineActionFeatures[subj.ID].append(subj.getSummaryRowList(logger,["ID", ListOfFeatrues], [], False))
#         Utils.exportList2Excel(rows, DIR + ListOfFeatrues + str(stopt) + ".csv")
# for part in list_of_participants:
#     Utils.exportList2Excel(OnlineActionFeatures[part], DIR + "ONL_ACT_FEAT_"+part+".csv")
#     list_of_outputs.append("ONL_ACT_FEAT_"+part+".csv")

