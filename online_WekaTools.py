'''
Created on Aug 2, 2013

@author: bondaria
'''
"""
The following is the code for repeated cross validation
"""
import weka_params
import os
import stattools

def Simple_Logistic_Regression_RepCV(directory, trainfile, testfile, resultfile):
    # directory - directory with input files
    # trainfile - name of trainfile
    # result file - name of result file
    cmd = weka_params.WEKAJAR+'\
         weka.classifiers.functions.SimpleLogistic -I 0 -M 500 -H 50 -W 0.0 \
         -t "'+ directory + trainfile + '" \
         -T "'+ directory + testfile + '" \
         -p 0  > "' + directory+resultfile + '"'
    os.popen(cmd)
    predicted = stattools.FindPredictedFromFile(directory, resultfile)
    return predicted

def Multilayer_Perceptron_RepCV(directory, trainfile, testfile, resultfile):
    cmd = weka_params.WEKAJAR+'\
         weka.classifiers.functions.MultilayerPerceptron -L 0.3 -M 0.2 -N 500 -V 0 -S 5 -E 20 -H a \
         -t "'+directory+trainfile+'" \
         -T "'+ directory + testfile + '" \
         -p 0  > "' + directory+resultfile + '"'
    os.popen(cmd)
    predicted = stattools.FindPredictedFromFile(directory, resultfile)
    return predicted

def Naive_Bayes_RepCV(directory, trainfile, testfile, resultfile):
    cmd = weka_params.WEKAJAR+'\
         weka.classifiers.bayes.NaiveBayes \
         -t "'+directory+trainfile+'" \
         -T "'+ directory + testfile + '" \
         -p 0  > "' + directory+resultfile + '"'
    os.popen(cmd)    
    predicted = stattools.FindPredictedFromFile(directory, resultfile)
    return predicted

def SMO_RepCV(directory, trainfile, testfile, resultfile):
    cmd = weka_params.WEKAJAR+'\
         weka.classifiers.functions.SMO -C 1.0 -L 0.001 -P 1.0E-12 -N 0 -V -1 -W 1 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0" \
         -t "'+directory+trainfile+'" \
         -T "'+ directory + testfile + '" \
         -p 0  > "' + directory+resultfile + '"'
    os.popen(cmd)    
    predicted = stattools.FindPredictedFromFile(directory, resultfile)
    return predicted


""" don't work in online case"""
def Random_Forest_RepCV(directory, trainfile, testfile, resultfile):
    cmd = weka_params.WEKAJAR+'\
         weka.classifiers.trees.RandomForest -I 20 -K 0 -S 1 \
         -t "'+directory+trainfile+'" \
         -T "'+ directory + testfile + '" \
         -p 0  > "' + directory+resultfile + '"'
    os.popen(cmd)
    TLL, FLL, THL, FHL = stattools.CalculateStatsFromFile(directory, resultfile)
    return TLL, FLL, THL, FHL
    
def MultinomialLogisticRegression_RepCV(directory, trainfile, testfile, resultfile):
    cmd = weka_params.WEKAJAR+'\
         weka.classifiers.functions.Logistic -R 1.0E-8 -M -1 \
         -t "'+directory+trainfile+'" \
         -T "'+ directory + testfile + '" \
         -p 0  > "' + directory+resultfile + '"'
    os.popen(cmd)
    TLL, FLL, THL, FHL = stattools.CalculateStatsFromFile(directory, resultfile)
    return TLL, FLL, THL, FHL


