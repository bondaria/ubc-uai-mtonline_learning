'''
Created on Sep 13, 2013
@author: bondaria

PURPOSE: Creates Tab Separated Files with Gaze and Actions features combined
'''

""" Absolute Paths to Actions Features and Gaze Features"""
DIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\McGill Project Data - online\\"
ACT_DIR = DIR + "OnlineActions\\"
GAZE_DIR = DIR + "OnlineGaze\\"
"""Output directory """
OUT_DIR = DIR + "OnlineFeatures\\"
TRAININGFILE = "trainingset"
""" Separators used in Actions and Gaze feature files"""
ACT_SEPARATOR = ","
GAZE_SEPARATOR = "\t"
OUT_SEPARATOR = "\t"

""" Prefix for Actions and Gaze"""
ACT_PREFIX ="ONL_ACT_FEAT_"
GAZE_PREFIX = "online_"
OUT_PREFIX = "online_full_"
""" Types of Actions and Gaze inputs"""
ACT_TYPE = ".csv"
GAZE_TYPE = ".tsv" 
OUT_TYPE = ".tsv"
""" 50 participants """
LIST_OF_PARTICIPANTS = ['MT208PN41005', 'MT208PN41008', 'MT208PN41009', 'MT208PN41010', 'MT208PN41011', 
                         'MT208PN41012', 'MT208PN41013', 'MT208PN41017', 'MT208PN41018', 'MT208PN41019', 
                         'MT208PN41020', 'MT208PN41023', 'MT208PN41024', 'MT208PN41027', 'MT208PN41029', 
                         'MT208PN41030', 'MT208PN41033', 'MT208PN41035', 'MT208PN41037', 'MT208PN41038', 
                         'MT208PN41039', 'MT208PN41040', 'MT208PN41041', 'MT208PN41042', 'MT208PN41043', 
                         'MT208PN41045', 'MT208PN41049', 'MT208PN41050', 'MT208PN41051', 'MT208PN41052', 
                         'MT208PN41053', 'MT208PN41054', 'MT208PN41058', 'MT208PN41059', 'MT208PN41060', 
                         'MT208PN41061', 'MT208PN41064', 'MT208PN41068', 'MT208PN41070', 'MT208PN41072', 
                         'MT208PN41076', 'MT208PN41080', 'MT208PN41081', 'MT208PN41082', 'MT208PN41084', 
                         'MT208PN41085', 'MT208PN41086', 'MT208PN41087', 'MT208PN41088', 'MT208PN41091']
trainingset = []
for part in LIST_OF_PARTICIPANTS:
    """Read action features """
    actinput = ACT_DIR + ACT_PREFIX + part + ACT_TYPE
    gazeinput = GAZE_DIR + GAZE_PREFIX + part + GAZE_TYPE
    with open(actinput, "r") as fact:
        act_lines = fact.readlines()
        act_len = len(act_lines)
    with open(gazeinput, "r") as fgaze:
        gaze_lines = fgaze.readlines()
        gaze_len = len(gaze_lines)
    out_lines = []
    min_len = min(gaze_len, act_len)
    for i in xrange(min_len):
        out_lines.append(act_lines[i].replace(ACT_SEPARATOR, OUT_SEPARATOR)[:-1]+ OUT_SEPARATOR + gaze_lines[i].replace(GAZE_SEPARATOR, OUT_SEPARATOR))
    out_lines[0] = out_lines[0].replace("ID", "pid") #Microsoft Excel Cannot read file as tsv if firts 2 symbols are ID 
    if trainingset == []:
        trainingset.append(out_lines[0])# add line with headers
    """ If gaze file is longer"""
    if gaze_len > act_len:
        print "WARNING: " + part + ", GAZE > ACTIONS"
        actline = act_lines[act_len - 1].replace(ACT_SEPARATOR, OUT_SEPARATOR)[:-1]
        for line in gaze_lines[min_len:]:
            out_lines.append(actline  + OUT_SEPARATOR + gaze_lines[i].replace(GAZE_SEPARATOR, OUT_SEPARATOR))
    else:
        gazeline = gaze_lines[i].replace(GAZE_SEPARATOR, OUT_SEPARATOR)
        for line in gaze_lines[min_len:]:
            out_lines.append(act_lines[i].replace(ACT_SEPARATOR, OUT_SEPARATOR)[:-1]  + OUT_SEPARATOR + gazeline)
    outfile =OUT_DIR + OUT_PREFIX + part + OUT_TYPE 
    trainingset.append(out_lines[-1])
    with open(outfile, "w") as fout:
        for line in out_lines:
            fout.write(line)
trainfile = OUT_DIR + TRAININGFILE + OUT_TYPE
with open(trainfile, "w") as ftrain:
    for line in trainingset:
        ftrain.write(line)