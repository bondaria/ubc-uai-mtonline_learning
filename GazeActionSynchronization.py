'''
Created on Sep 10, 2013
This module contains functions & code needed for synchronization of Gaze and Action Logs
@author: bondaria
'''

LOGDIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\MetaTutor-source\\McGill\\MTLogAnalyzer\\MetaTutor\\LOGS-12-50p\\"
DATADIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\MetaTutor-source\\McGill\\MetaTutor\\AIED2013-data\\McGill Project Data\\"
OUTDIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\MetaTutor-source\\McGill\\MetaTutor\\AIED2013-data\\MTAux\\"

OUTFILE  = OUTDIR + "OFFSETS.txt"
""" position of various relevant information in log files """

""" Action Logs Related constants"""
DAY_LABEL_POS = 7 
SUBJECT_ID_POS = 2
LOG_LASTACTION_POS = -1

""" Gaze data related settings """
""" None Yet """
GAZE_FIRSTACT_POS = 4
# sample for ACTIONS + GAZE 
list_of_participants = ['MT208PN41005', 'MT208PN41008', 'MT208PN41009', 'MT208PN41010', 'MT208PN41011', 
                         'MT208PN41012', 'MT208PN41013', 'MT208PN41017', 'MT208PN41018', 'MT208PN41019', 
                         'MT208PN41020', 'MT208PN41023', 'MT208PN41024', 'MT208PN41027', 'MT208PN41029', 
                         'MT208PN41030', 'MT208PN41033', 'MT208PN41035', 'MT208PN41037', 'MT208PN41038', 
                         'MT208PN41039', 'MT208PN41040', 'MT208PN41041', 'MT208PN41042', 'MT208PN41043', 
                         'MT208PN41045', 'MT208PN41049', 'MT208PN41050', 'MT208PN41051', 'MT208PN41052', 
                         'MT208PN41053', 'MT208PN41054', 'MT208PN41058', 'MT208PN41059', 'MT208PN41060', 
                         'MT208PN41061', 'MT208PN41064', 'MT208PN41068', 'MT208PN41070', 'MT208PN41072', 
                         'MT208PN41076', 'MT208PN41080', 'MT208PN41081', 'MT208PN41082', 'MT208PN41084', 
                         'MT208PN41085', 'MT208PN41086', 'MT208PN41087', 'MT208PN41088', 'MT208PN41091']
#list_of_participants = ['MT208PN41009']

from datetime import time 
import os

def time_to_milisecs(atime):
    """ Converts from time (built-in format) to milisecs"""
    milisec = atime.microsecond/1000 + 1000*(atime.second + 60*(atime.minute + 60*atime.hour))
    return milisec
def string_to_time(strtime):
    """ Converts from string to time (built-in format) """
    timelist = strtime.split(':')
    time_digits = len(timelist)
    hour = int(timelist[0])
    min = int(timelist[1])
    sec = int(timelist[2])
    if time_digits > 3:
        microsec = int(timelist[3])*1000
        return time(hour, min, sec, microsec)
    else:
        return time(hour, min, sec, 0)

def CalculateLogStartEnd(filename):
    '''
    CalculateLogStartEnd calculates absolute time of start and end of the log 
    filename - input logfile
    
    output:
    list of two elements:
    1 - subject ID (string)
    2 - list (pair) with first action and last action [and  relative timestamp of the first action recorded]
    
    '''
    with open(filename, "r") as flog:
        lines = flog.readlines()
        dummypos = lines[DAY_LABEL_POS]
        if (lines[DAY_LABEL_POS].find("Day") == -1): 
            '''if Day is not located at the usual place'''             
            raise Exception("No Day specification")
        else:
            if (lines[DAY_LABEL_POS].find("1") != -1):
                '''if Day 1 Log  - break'''
                return []
            else:
                '''if Day 2 Log  - continue'''
                subjId = lines[SUBJECT_ID_POS].split("\t")[1]
                if subjId[-1] == "\n":
                    subjId = subjId[:-1]#get rid of transfering to the new line
                firstaction = string_to_time(lines[DAY_LABEL_POS + 1].split("\t")[1])
                firstaction_rel =  int(lines[DAY_LABEL_POS + 1].split("\t")[2])
                lastaction = string_to_time(lines[LOG_LASTACTION_POS].split("\t")[1])
                return [subjId, [firstaction, lastaction, firstaction_rel]]
    
def CalculateGazeStart(filename):
    '''
    This function calculates start and end of the gaze interaction captured by eye-tracker based on fixation file input
    Input: filename - path + filename of the fixation file
    output:
    list of two elements:
    1 - subject ID (string)
    2 - list (pair) with first action and last action
    '''
    with open(filename, 'r') as fgaze:
        for i in xrange(GAZE_FIRSTACT_POS):
            line = fgaze.readline()
        timeline = line[16:28]#corresponds to 0 timestamp for Eye-tracking data
    return string_to_time(timeline)

""" Creating list action logs"""
list_of_logs = os.listdir(LOGDIR)
list_of_logs = filter(lambda y: y[-4:] == '.log', list_of_logs) 

print list_of_logs
print "Number of log files found: ", len(list_of_logs)

# """ Creating list of gaze actions """
# list_of_actions = os.listdir(DATADIR)
# list_of_actions = filter(lambda y: y.find("-Fixation-Data.tsv")>0 and y[-4:] == '.tsv', list_of_actions)
# print list_of_actions
# print "Number of gaze data files found: ", len(list_of_actions)

dictActStart = {}
for log in list_of_logs:
    curSubj =  CalculateLogStartEnd(LOGDIR + log)
    if curSubj != []:
        dictActStart[curSubj[0]] = curSubj[1]
print "Processing action logs: "
print dictActStart
print "Number of subjects processed: ", len(dictActStart.keys())

dictGazeStart = {}
for part in list_of_participants:
    fixationfile = DATADIR + part + "-Fixation-Data.tsv"
    dictGazeStart[part] = CalculateGazeStart(fixationfile)
print "Processing Gaze logs"
print dictGazeStart
print "Subjects Gaze data processed for: ", len(dictGazeStart.keys())



print "Calculating offsets:"
dictOffsets = {}
for part in list_of_participants:
    """ offset is calculated as difference of the first action in log files and 
    relative timestamp of the first action (to calculate the start of action log) 
    and start of gaze log (eye tracker started recording data before MetaTutor start)
    OPTIONAL: add 500ms to Action Log time (absolute time in Action Logs is given up to seconds, Eye tracker first time stamp is available in miliseconds )
    """
    dictOffsets[part] = {}
    dictOffsets[part]["offset"] = time_to_milisecs(dictActStart[part][0]) - time_to_milisecs(dictGazeStart[part]) - dictActStart[part][2] #+ 500
    dictOffsets[part]["session end"] = dictActStart[part][1]
print dictOffsets

with open(OUTFILE, "w") as fout:
    for part in list_of_participants:
        line = part + "\t" + str(dictOffsets[part]["offset"]) + "\n"
        fout.write(line)
    
#fcsv = CalculateLogStartEnd(LOGDIR + "MTM2a_41064_2012-06-08_11-45.log")
