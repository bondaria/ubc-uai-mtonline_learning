'''
Created on May 11, 2010

@author: skardan
'''
from numpy import shape, empty
def addColumnToEnd(mat,col):
    res=[]
    k=0
    for l in mat:
        res.append([])
        res[k].extend(l)
        res[k].append(col[k])
        k+=1
    return res
def addColumnToBeginning(mat,col):
    res=[]
    k=0
    for l in mat:
        res.append([col[k]])
        res[k].extend(l)
        k+=1
    return res

def select_feat(points,features):
    """
    returns a subset of points which only includes features in features
    """
    print "(points):",shape(points)
    print "features:",features
    res = empty((shape(points)[0],len(features)))
    i=0
    print features
    for f in features:
        print "feature",f
        res [:,i]= points[:,f]
        i+=1

    #print "(res):",shape(res)
    #print res
    
    return res

def select_feat_for_lists(points,features):
    """
    Daria
    returns a subset of points which only includes features in features
    """
    #print "(points):",shape(points)
    #print "features:",features
    res = empty((shape(points)[0],len(features)))
    i=0
    #print features
    for f in features:
        #print "feature",f
        for el in xrange(shape(points)[0]):
            res [el][i]= points[el][f]
        i+=1

    #print "(res):",shape(res)
    #print res
    
    return res
def create_list_of_features(requiredfeatures, fulllist):
    """
    Daria
    
    this function produces list of indexes that should be selected for the current Method 
    
    """
    res = []
    for el in requiredfeatures:
        if el in fulllist:
            res.append(fulllist.index(el))
        else:
            print "Warning: Feature '" + el + "' is not in the list"
    return res


def remove_feat(points,feature):
    #print "(points):",shape(points)
    res = empty((shape(points)[0],shape(points)[1]-1))
    #print "(res):",shape(res)
    #print "feature",feature
    res [:,0:feature]= points[:,0:feature]
    res [:,feature:]= points[:,feature+1:]
    
    return res

###
#    a=[[1,2],[3,4],[5,6]]
#    b=['a','b','c']
#    
#    print addColumnToEnd(a,b)
#    print a
#    print addColumnToBeginning(a,b)
#    print a