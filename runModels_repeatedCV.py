'''
Created on Aug 2, 2013
This module is a part of   online learning simulation package

@author: bondaria
'''
DISTR = [[2, 2, 2, 2, 2, 3, 3, 3, 3, 3], [3, 3, 3, 3, 3, 2, 2, 2, 2, 2]]
CLASSES = 2
import random
from online_WekaTools import *
from arfftools import findStartOfData
from stattools import *
import scipy as sc
import re
import numpy as np


from MetaTutor_file_tools import load_all_stats_from_tsv, load_user_stats_from_tsv, embed_in_arff_featnames_label
import weka_params
from mattools import addColumnToEnd, select_feat_for_lists, create_list_of_features

def GenerateDictClasses(list_of_participants, list_of_labels, dict_of_labels):
    """
    Creates dictionary with lists of participants that correspond to different folds
    """
    print "Generating class elements lists"
    
    dict_classes = {}
    for el in dict_of_labels.keys():
        dict_classes[dict_of_labels[el]] = []
    for i in xrange(len(list_of_participants)):
        pid = list_of_participants[i]
        pid_class = dict_of_labels[list_of_labels[i]]
        dict_classes[pid_class].append(pid)
    
    return dict_classes 

def GenerateDictClassesFromARFF(INPUT, CLASSLABELS = ["LL", "HL"]):
    """
    Creates dictionary with elements of CLASSLABELS as keys and 
    list of all elements from INPUT finle  with matching label
    
    Note:this function works only with 2-letter labels. Otherwise should be changed
    """
    print "Generating class elements lists"
    with open(INPUT, 'r') as f: #read file
        lines = f.readlines()
    curstr = findStartOfData(INPUT)
    dict_classes = {}
    for el in CLASSLABELS:
        dict_classes[el] = []
    i = 0
    while curstr < len(lines):
        line = lines[curstr]
        dict_classes[line[-3:-1]].append(i) # !!!!!! change this line if you have other labels
        i += 1
        curstr += 1
    return dict_classes 

def GenerateFolds_for_nruns(class_LL, class_HL, distr, N, K):
    """
    Generates random folds given 
    """
    rnd = random.Random()
    runfolds = []
    for run in xrange(N):
        rnd.seed(run + 8)
        folds = []
        curLL = 0
        curHL = 0
        #print "Current Run is " + str(run)
        rnd.shuffle(class_LL)
        rnd.shuffle(class_HL)
        Class1_i = run % 2
        Class2_i = 1 - (run % 2)
        for numfold in xrange(K):
            curfold = []
            #print "Cur Distr is: " + str(distr[Class1_i][numfold]) + " + "+ str(distr[Class2_i][numfold])
            for j in xrange(distr[Class1_i][numfold]):
                curfold.append(class_LL[curLL])
                curLL += 1
            for j in xrange(distr[Class2_i][numfold]):
                curfold.append(class_HL[curHL])
                curHL += 1
            folds.append(curfold)
        #print "FOLDS:"
        #for fold in folds:
        #    print fold 
        #check of intersections:
        for i in xrange(K):
            for j in xrange(K):
                if j > i:
                    if [val for val in folds[i] if val in folds[j]] != []:
                        raise RuntimeError("Sampling process is incorrect. Check code!")
    #                 else:
    #                     print "All is OK. Proceed"
        runfolds.append(folds)
    return runfolds


def CreateSamplesStratifiedFromUserList(N,K, list_of_participants = weka_params.ul, list_of_classes = weka_params.CLASS, class_labels = weka_params.CLASS_LABELS):
    """
    This function creates folds for N-repeated K-fold Stratified Cross Validation 
    (Stratified - samples are created balanced in terms of class distribution)
    Input:
    N - number of runs in Repeated CV
    K - number of folds in Repeated CV
     
    """
    if (N ==10) and (K==10):
        dict_classes = GenerateDictClasses(list_of_participants, list_of_classes, class_labels)
        #dict_classes = GenerateDictClasses(INPUT, CLASSLABELS = ["LL", "HL"])
        return GenerateFolds_for_nruns(dict_classes["LL"], dict_classes["HL"], DISTR, N, K)
    else:
        raise RuntimeError('current implementation supports only 10-fold cross Validation. Sorry for inconvenience')
    return

def CreateTrainFromTsv(DIR, INPUT, testfold, featurelabels, trainFileName, DIRTRAIN=None, list_of_participants = weka_params.ul, classlist = weka_params.CLASS, classlabels = weka_params.CLASS_LABELS):
    """
    PURPOSE: create train data from a single tsv file 
    INPUTS:
        DIR - directory with input file
        INPUT - file with features to be used for modeling
        testfold - list of ID of participants that from test fold (should be ignored in train file)
        featurelabels - labels of features to be exported
        trainFileName - name of the train file to be created
        DIRTRAIN - directory to store the train file. If not specified - DIR is used instead
        list_of_participants -
        
    INPUT file contains 1 line with list of features. The rest has features for the whole sample
    """
    usertraindata = []
    if DIRTRAIN is not None:
        trainfile = open(DIRTRAIN + trainFileName,"w")
    else: 
        """IF DIRTRAIN is not specified DIR is used instead"""
        trainfile = open(DIR + trainFileName,"w")
    traindata = []
    with open(INPUT, 'r') as f:
        lines = f.readlines()
        
    fullfeatures = lines[0][:-1].split("\t")
    print fullfeatures
    
    featnums = []
    
    """ create list of indexes of featurelabels to be exported """
    for feat in featurelabels:
        featnums.append(fullfeatures.index(feat))
    print "Number of participants ", str(len(lines[1:]))
    data, userlist, fullfeatures = load_all_stats_from_tsv(len(list_of_participants), [], INPUT)
    featnums = create_list_of_features(featurelabels, fullfeatures)
    data = select_feat_for_lists(data,featnums)
    userclasslabels = []
    for dataline, pid in zip(data, userlist):
        if pid not in testfold:
            """ add class label"""
            userclass = classlabels[classlist[list_of_participants.index(pid)]]
            userclasslabels.append(userclass)
            traindata.append(dataline)
        else:
            print "Participant ", pid, "was excluded"
    traindata = addColumnToEnd(traindata, userclasslabels) 

    result = embed_in_arff_featnames_label(featurelabels, traindata, relation_name = "online_learning_train", class_labels = classlabels)
    
    for line in result:
        trainfile.write(line)
    trainfile.close()
    return True

def CreateTestFromTSVs(DIR, testfold, featurelabels, testFileName, input_prefix = weka_params.USERDATAFILE_PREFIX, DIRTEST=None, list_of_participants = weka_params.ul, classlist = weka_params.CLASS, classlabels = weka_params.CLASS_LABELS):
    """
    PURPOSE: Create a test file from a set of files 
    INPUT:
    OUTPUT:
    
    """
    if DIRTEST is not None:
        testfile = open(DIRTEST + testFileName,"w")
    else: 
        """IF DIRTRAIN is not specified DIR is used instead"""
        testfile = open(DIR + testFileName,"w")
    
    user_intervals = []
    testdata = []
    featnums = []
    for user in testfold:       
        userfile = input_prefix + user+".tsv"
        print "userfile: ", userfile
        sectfeats, usersections, fullfeatures = load_user_stats_from_tsv(userfile, first_Col_is_label = False)
        userclasslabel = classlabels[classlist[list_of_participants.index(user)]]
        labels = [userclasslabel]*(len(usersections))
        
        featnums = create_list_of_features(featurelabels, fullfeatures)
        sectfeats = select_feat_for_lists(sectfeats,featnums)
        
        
        user_intervals.append(len(usersections))
        usertestdata=addColumnToEnd(sectfeats,labels)
        
        testdata.extend(usertestdata)
     
    
    """ print result to the output file """
    result = embed_in_arff_featnames_label(featurelabels, testdata, relation_name = "online_learning_test", class_labels = classlabels)
    for line in result:
        testfile.write(line)
    
    testfile.close()
    return user_intervals

def CreateTrainTest(DIR, INPUT, testfold, trainFileName, testFileName,DIRTRAIN=None):
    """
    PURPOSE: Create Train and Test Files from given .arff file (both train and test are created from a specific file)
    DIR+INPUT = path to the input .arff file
    testfold - ids of elements in INPUT that should be added to the test file
    Everything not included to testfold will be written in trainfile
    DIRTRAIN - directory for writing Test and Train files. DIR is used if DIRTRAIN is not specified
    """
    
    #trainFileName = INPUT[:-5]+"-train" + suffix +".arff"
    #testFileName = INPUT[:-5]+"-test"+ suffix +".arff"
    if DIRTRAIN is not None:
        trainfile = open(DIRTRAIN + trainFileName,"w")
        testfile = open(DIRTRAIN + testFileName,"w")
    else: 
        """IF DIRTRAIN is not specified DIR is used instead"""
        #print "creating Test and Train"
        trainfile = open(DIR + trainFileName,"w")
        testfile = open(DIR + testFileName,"w")
    with open(DIR + INPUT, 'r') as f:
        lines = f.readlines()
        featstart = findStartOfData(DIR + INPUT)
        for curstr in xrange(featstart):
            trainfile.write(lines[curstr])
            testfile.write(lines[curstr])
        i = 0
        for curstr in range(featstart,len(lines)):
            if i in testfold:
                testfile.write(lines[curstr])
            else:
                trainfile.write(lines[curstr])
            i += 1
            curstr += 1
    testfile.close()
    trainfile.close()
    
def runClassifierWithRepeatedCV(DIR, dict_Classifiers, N, K, CVSamples, Ensemble = False, CombMethod = "Majority"):
    #def runClassifierWithRepeatedCV(DIR, INPUT, Classifier, N, K, Ensemble = False):
    """
    This function trains classifier and tests it with N run K fold CV
    Inputs:
    DIR - directory
    dict_Classifiers - dictionary with Algorithms as keys and lists of corresponding 
    """
    run_TLL = []
    run_FLL = []
    run_THL = []
    run_FHL = []
    run_acc = []
    run_LL_acc = []
    run_HL_acc = []
    run_kappa = []
    
    """ one run of CV"""
    for currun in CVSamples: #for each run
        dict_predicted_results = {}
        #for each run calculate aggregated stats. 
        curTLL = []
        curFLL = [] 
        curTHL = [] 
        curFHL = []
        if Ensemble is False: #if we are working with regular classifier
            for curfold in currun:
                    dict_predicted_curfold = AnyClassifierOnline(DIR, weka_params.DATAFILE, dict_Classifiers, curfold)
                        
                    for pid in curfold:
                        dict_predicted_results[pid] = dict_predicted_curfold[pid]
        else:
            for curfold in currun:
                dict_predicted_curfold = EnsembleClassiferOnline(DIR, weka_params.DATAFILE, dict_Classifiers, curfold)
                
                for pid in curfold:
                        dict_predicted_results[pid] = dict_predicted_curfold[pid]
        
        dict_mapped_100 = MapSectionsTo100(dict_predicted_results)            
        curTLL, curFLL, curTHL, curFHL = ClaculateStatsFromMappedDict(weka_params.CLASS, dict_mapped_100, weka_params.ul)
        
        run_TLL.append(curTLL)    
        run_FLL.append(curFLL)
        run_THL.append(curTHL)
        run_FHL.append(curFHL)
         
        acc_overall = [0] * 100
        acc_LL = [0] * 100
        acc_HL = [0] * 100
        kappa_overall = [0] * 100
        
        for i in xrange(100):
            acc_overall[i], acc_LL[i], acc_HL[i], kappa_overall[i] = processOneRun(curTLL[i], curFLL[i], curTHL[i], curFHL[i])
        run_acc.append(acc_overall)
        run_LL_acc.append(acc_LL)
        run_HL_acc.append(acc_HL)
        run_kappa.append(kappa_overall)
#     print "TLL", run_TLL
#     print "FLL", run_FLL
#     print "THL", run_THL
#     print "FHL", run_FHL
#     print "Overall Accuracy: ", run_acc
#     print "Overall Accuracy: ", 1.0 * sum(run_acc) / len(run_acc)
#     print "LL Accuracy: ", run_LL_acc
#     print "LL Accuracy: ", 1.0 * sum(run_LL_acc) / len(run_LL_acc)
#     print "HL Accuracy: ", run_HL_acc
#     print "HL Accuracy: ", 1.0 * sum(run_HL_acc) / len(run_HL_acc)
#     print "Average Kappa: ", run_kappa
#     print "Average Kappa: ", 1.0 * sum(run_kappa) / len(run_kappa)
    
    final_acc = [0] * 100
    final_LL_acc = [0] * 100
    final_HL_acc = [0] * 100
    final_kappa = [0] * 100
    
    MSE = [-99] * 100 # not calculated yet
    final_var = [-99] * 100 # not calculated yet
    for j in xrange(100):
        sum_run_acc = 0
        sum_run_LL_acc = 0
        sum_run_HL_acc = 0
        sum_run_kappa = 0
        for i in xrange(N):
            print "(i,j): " + str(i) + ", " + str(j) 
            sum_run_acc += run_acc[i][j]
            sum_run_LL_acc += run_LL_acc[i][j]
            sum_run_HL_acc += run_HL_acc[i][j]
            sum_run_kappa += run_kappa[i][j]
        final_acc[j] = 1.0 * sum_run_acc / N
        final_LL_acc[j] = 1.0 * sum_run_LL_acc / N
        final_HL_acc[j] = 1.0 * sum_run_HL_acc / N
        final_kappa[j] = 1.0 * sum_run_kappa / N
        
        """
        MSE = 0
        for el in run_acc:
            MSE += (el - final_acc) ** 2
        
        final_var = sc.math.sqrt((float(MSE))/len(run_acc))
        """
        
    return final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa


def MapSectionsTo100(dict_predicted_results, USERS = weka_params.ul, NUSERS = weka_params.USERS, usersclasses = weka_params.CLASS):
    """
    PURPOSE:
    Maps dictionary of results to 100 intervals
    INPUT:
    dict_predicted_results dictionary with users PID as keys and a list of predicted results as value
    OUTPUT:
    """
    results_mapped = {}  
    for pid in USERS:
        results_mapped[pid] = []
        num_of_sections = len(dict_predicted_results[pid])
        index = 0
        for j in xrange(1,101):
            if j<(100 + weka_params.CUTOFF):
                index = int((j * num_of_sections /(100 + weka_params.CUTOFF)))+1
            else:
                index = int(int(j * num_of_sections / (100 + weka_params.CUTOFF)))
            results_mapped[pid].append(dict_predicted_results[pid][index - 1])
            #if usersclasses[USERS.index(pid)] == dict_predicted_results[pid][index - 1]:
            #    results_mapped[pid].append(0)
            #else:
            #    results_mapped[pid].append(1)
    return results_mapped

def AnyClassifierOnline(DIR,INPUT, dict_Features, curfold):
    """
    PURPOSE: runs all models listed in dict_classifiers. The model is built for a particular fold curfold
    INPUT: dict_Classifiers. Keys - ALGORITHMS, Values - lists of input values 
    curfold - list of participants in the current fold
    OUTPUT: dictionary of predicted values. Keys = user names. Values: list of predicted values for all sessions 
    """
    #resfiles = []
    predicted = []
    dict_online_result = {}
    Classifier = dict_Features.keys()[0]
    dirtrain = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\McGill Project Data - online\\_for_testing_only\\"
    print dict_Features
    print Classifier
    featurelabels = dict_Features[Classifier]
    trainFileName = "train_"+Classifier+".arff"
    testFileName = "test_"+Classifier+".arff"
    resultFileName = "result"+Classifier+".txt"
    #resfiles.append(resultFileName)
    
    """ For online learning train and test should be created separately: """
    CreateTrainFromTsv(DIR, INPUT, curfold, featurelabels, trainFileName, DIRTRAIN=dirtrain, list_of_participants = weka_params.ul, classlist = weka_params.CLASS, classlabels = weka_params.CLASS_LABELS)
    curfold_sections = CreateTestFromTSVs(DIR, curfold, featurelabels, testFileName, input_prefix = weka_params.USERDATAFILE_PREFIX, DIRTEST=dirtrain, list_of_participants = weka_params.ul, classlist = weka_params.CLASS, classlabels = weka_params.CLASS_LABELS)
    
    exec ("predicted = " + Classifier + "(dirtrain, trainFileName, testFileName, resultFileName)")
    curpos = 0 # position in predicted
    for user, sectionnum in zip(curfold, curfold_sections):
        dict_online_result[user] = predicted[curpos:curpos+sectionnum]
        curpos += sectionnum
        
    return dict_online_result

            

    
def EnsembleClassiferOnline(DIR, INPUT, dict_Classifiers, curfold):
    """
    PURPOSE: 
        ENSEMBLE MODELING ONLINE!
        this method builds ensemble classifier using models listed in Classifiers.
    INPUT:
        DIR - current directory with related files
        dict_Classifiers is a dictionary of classifiers used in model building with classifier names as keys and lists of lists of labels as values.  
        curfold - current fold (list of # that form test set. Everything else forms train set)
    OUTPUT:
        dictionary of results with IDs of participants as keys and and lists of predicted values for all participants as values
    
    """
    dict_online_result = {}
    resfiles = []
    predicted = []
    all_predicted = []
    Classifiers = dict_Classifiers.keys()
    dirtrain = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\McGill Project Data - online\\_for_testing_only\\"
    outid = 0
    for Classifier in Classifiers:
        for featurelabels in dict_Classifiers[Classifier]:
            trainFileName = "train_"+Classifier+".arff"
            testFileName = "test_"+Classifier+".arff"
            resultFileName = "result" + str(outid) + ".txt"
            outid += outid
            resfiles.append(resultFileName)
            
            CreateTrainFromTsv(DIR, INPUT, curfold, featurelabels, trainFileName, DIRTRAIN=dirtrain, list_of_participants = weka_params.ul, classlist = weka_params.CLASS, classlabels = weka_params.CLASS_LABELS)
            curfold_sections = CreateTestFromTSVs(DIR, curfold, featurelabels, testFileName, input_prefix = weka_params.USERDATAFILE_PREFIX, DIRTEST=dirtrain, list_of_participants = weka_params.ul, classlist = weka_params.CLASS, classlabels = weka_params.CLASS_LABELS)
            
            exec ("predicted = " + Classifier + "(dirtrain, trainFileName, testFileName, resultFileName)")
            curpos = 0 # position in predicted
            all_predicted.append(predicted)
   
    predicted = Ensemble_Classifier_process_majority(all_predicted)
    
    for user, sectionnum in zip(curfold, curfold_sections):
        dict_online_result[user] = predicted[curpos:curpos+sectionnum]
        curpos += sectionnum

    return dict_online_result

def Ensemble_Classifier_process_majority(list_of_models_results):
    """
    PURPOSE:
        Processes output from EnsembleClassifier 
    INPUT:
        list_of_models_results - list of predicted values from all classifiers that participate in ensemble.
    OUTPUT:
        List of predicted values for the current classifier
    
    """
    
    number_of_sections = len(list_of_models_results[0])
    results = [-1] * number_of_sections
    resEnsemble = np.zeros([number_of_sections, CLASSES]) #<2 lines correspond to two classes>
    
    for i in xrange(number_of_sections):
        for one_model in list_of_models_results:
            predicted_class = one_model[i]
            resEnsemble[i][predicted_class] += 1
            
        results[i] = np.argmax(resEnsemble[i])
        
    
    
    return results



