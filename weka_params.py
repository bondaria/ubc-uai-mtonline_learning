'''
Created on 2013-01-12
This file contains set of parameters that are required to call WEKA
THIS IS NOT EMDAT PARAMS FILE 

@author: Daria


params.PLG                      a list of proportional learning gains for users
params.USERS                    number of users
params.OUTLIERS                 list of users that are outliers can be empty []
params.K                        number of classes e.g. k = 2
params.DATAFILE                 a .csv file containing one vector for each user with features calculated at the end of interaction (train data) - FULL!!!!!
params.USERDATAFILE_PREFIX      the path and prefix for the name of user data files. Each file contains a number of vectors, one per line for each time stamp during the 
                                interaction for a user (e.g. after every 30 seconds or after every 10% of interaction)
params.ul                       list of users could be [1,2,...,n] or non-continues numbers this number will be added to the end of params.USERDATAFILE_PREFIX to form the user data file name
params.first_Col_is_label       a Boolean specifying whether the first column in the user data file (test) is the user label and should be ignored
 

'''
WEKAJAR = 'java -cp "C:\Program Files\Weka-3-6\weka.jar"'
CUTOFF = 0.0

OUTLIERS = []

K = 2
#DATADIR = "D:\\workspace\\MetaTutor\\data\\McGill Project Data\\Online-test\\"
DATADIR = "C:\\Users\\bondaria\\Documents\\UAI\\GENOVA\\McGill Project Data - online\\OnlineFeatures\\"
DATAFILE = DATADIR + "trainingset.tsv"
USERDATAFILE_PREFIX = DATADIR + "online_full_" # +participant name +.tsv

#USERS = 3
#ul = ['MT208PN41005','MT208PN41008','MT208PN41009']
#PLG = [25, 50, 33.33]

#AIED2013
# USERS = 47
# ul = ['MT208PN41005', 'MT208PN41008', 'MT208PN41009', 'MT208PN41010', 'MT208PN41011', 'MT208PN41012', 'MT208PN41013', 'MT208PN41017', 'MT208PN41018', 'MT208PN41019', 'MT208PN41020', 'MT208PN41023', 'MT208PN41024', 'MT208PN41027', 'MT208PN41029', 'MT208PN41030', 'MT208PN41033', 'MT208PN41035', 'MT208PN41037', 'MT208PN41038', 'MT208PN41039', 'MT208PN41040', 'MT208PN41041', 'MT208PN41042', 'MT208PN41043', 'MT208PN41045', 'MT208PN41049', 'MT208PN41050', 'MT208PN41051', 'MT208PN41052', 'MT208PN41054', 'MT208PN41058', 'MT208PN41059', 'MT208PN41060', 'MT208PN41061', 'MT208PN41068', 'MT208PN41070', 'MT208PN41072', 'MT208PN41075', 'MT208PN41076', 'MT208PN41081', 'MT208PN41084', 'MT208PN41085', 'MT208PN41086', 'MT208PN41087', 'MT208PN41088', 'MT208PN41091']
# PLG = [25, 50, 33.33333333, -14.28571429, 57.14285714, 0, -100, 33.33333333, 66.66666667, 33.33333333, 0, 80, 50, 100, 75, 25, 18.18181818, 66.66666667, 100, 75, 57.14285714, 60, 83.33333333, 57.14285714, 50, 25, 33.33333333, 54.54545455, 83.33333333, 60, -66.66666667, 0, 50, 0, -50, 50, -50, 64.28571429, 33.33333333, 0, 62.5, 0, 100, 0, 55.55555556, 33.33333333, 9.090909091]
# CLASS = [0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0]

#thesis and final
USERS = 50

ul = ['MT208PN41005', 'MT208PN41008', 'MT208PN41009', 'MT208PN41010', 'MT208PN41011', 
         'MT208PN41012', 'MT208PN41013', 'MT208PN41017', 'MT208PN41018', 'MT208PN41019', 
         'MT208PN41020', 'MT208PN41023', 'MT208PN41024', 'MT208PN41027', 'MT208PN41029', 
         'MT208PN41030', 'MT208PN41033', 'MT208PN41035', 'MT208PN41037', 'MT208PN41038', 
         'MT208PN41039', 'MT208PN41040', 'MT208PN41041', 'MT208PN41042', 'MT208PN41043', 
         'MT208PN41045', 'MT208PN41049', 'MT208PN41050', 'MT208PN41051', 'MT208PN41052', 
         'MT208PN41053', 'MT208PN41054', 'MT208PN41058', 'MT208PN41059', 'MT208PN41060', 
         'MT208PN41061', 'MT208PN41064', 'MT208PN41068', 'MT208PN41070', 'MT208PN41072', 
         'MT208PN41076', 'MT208PN41080', 'MT208PN41081', 'MT208PN41082', 'MT208PN41084', 
         'MT208PN41085', 'MT208PN41086', 'MT208PN41087', 'MT208PN41088', 'MT208PN41091']
PLG = [25,50,33.33333333,-14.28571429,57.14285714,0,-100,33.33333333,66.66666667,33.33333333,0,80,50,100,75,25,18.18181818,66.66666667,100,75,57.14285714,60,83.33333333,57.14285714,50,25,33.33333333,54.54545455,83.33333333,60,25,-66.66666667,0,50,0,-50,45.45454545,50,-50,64.28571429,0,37.5,62.5,33.33333333,0,100,0,55.55555556,33.33333333,9.090909091]
CLASS = [0,1,0,0,1,0,0,0,1,0,0,1,1,1,1,0,0,1,1,1,1,1,1,1,1,0,0,1,1,1,0,0,0,1,0,0,1,1,0,1,0,0,1,0,0,1,0,1,0,0]

CLASS_LABELS = {}
CLASS_LABELS[0] = "LL"
CLASS_LABELS[1] = "HL"


#USERS = 46
#ul = ['MT208PN41008', 'MT208PN41009', 'MT208PN41010', 'MT208PN41011', 'MT208PN41012', 'MT208PN41013', 'MT208PN41017', 'MT208PN41018', 'MT208PN41019', 'MT208PN41020', 'MT208PN41023', 'MT208PN41024', 'MT208PN41027', 'MT208PN41029', 'MT208PN41030', 'MT208PN41033', 'MT208PN41035', 'MT208PN41037', 'MT208PN41038', 'MT208PN41039', 'MT208PN41040', 'MT208PN41041', 'MT208PN41042', 'MT208PN41043', 'MT208PN41045', 'MT208PN41049', 'MT208PN41050', 'MT208PN41051', 'MT208PN41052', 'MT208PN41054', 'MT208PN41058', 'MT208PN41059', 'MT208PN41060', 'MT208PN41061', 'MT208PN41068', 'MT208PN41070', 'MT208PN41072', 'MT208PN41075', 'MT208PN41076', 'MT208PN41081', 'MT208PN41084', 'MT208PN41085', 'MT208PN41086', 'MT208PN41087', 'MT208PN41088', 'MT208PN41091']
#PLG = [ 50, 33.33333333, -14.28571429, 57.14285714, 0, -100, 33.33333333, 66.66666667, 33.33333333, 0, 80, 50, 100, 75, 25, 18.18181818, 66.66666667, 100, 75, 57.14285714, 60, 83.33333333, 57.14285714, 50, 25, 33.33333333, 54.54545455, 83.33333333, 60, -66.66666667, 0, 50, 0, -50, 50, -50, 64.28571429, 33.33333333, 0, 62.5, 0, 100, 0, 55.55555556, 33.33333333, 9.090909091]
#CLASS = [ 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0]

#list of selected features for simple logistic regression

featSLR_labels = ['Subgoals_proptransfrom_TableOfContents', 'Subgoals_proptransfrom_TextContent', 'Subgoals_proportionnum_dynamic', 'Subgoals_proptransfrom_LearningStrategiesPalette', 'Subgoals_proportiontime_dynamic', 'LearningStrategiesPalette_proptransfrom_ImageContent', 'stddevfixationduration', 'LearningStrategiesPalette_proptransfrom_TableOfContents', 'meanfixationduration', 'fixationrate', 'meanrelpathangles', 'OverallLearningGoal_proptransfrom_TableOfContents', 'OverallLearningGoal_longestfixation', 'TableOfContents_proptransfrom_TextContent']

""" feature labels for best performing models """
feat_SLR_Gaze_labels = ["meanrelpathangles", "Subgoals_proptransfrom_TableOfContents", "LearningStrategiesPalette_proptransfrom_Agent", "TableOfContents_proptransfrom_TableOfContents", "TextContent_fixationrate", "TextContent_proptransfrom_Agent", "TextContent_proptransfrom_ImageContent", "TextContent_proptransfrom_TableOfContents", "ImageContent_fixationrate", "ImageContent_proptransfrom_Subgoals"]

feat_SLR_Full_labels = ["stddevabspathangles", "OverallLearningGoal_proptransfrom_TableOfContents", "Subgoals_proportionnum_dynamic", "Subgoals_proptransfrom_Agent", "Subgoals_proptransfrom_TableOfContents", "LearningStrategiesPalette_longestfixation", "TextContent_proportionnum_dynamic", "TimeSpentWithContentOverall", "NoteCheckingDuration", "AVGNumSRLpePagetoSG0AnyTime"]

feat_SLR_Full_labels_final = ["Subgoals_proptransfrom_TableOfContents", "TableOfContents_proptransfrom_TableOfContents", "TextContent_proptransfrom_ImageContent", "ImageContent_proptransfrom_ImageContent", "NoteCheckingNum", "TimePKA", "RatioPagesRelevantToInitialSubgoal1ReadWhenSubgoal1Active", "numJOL", "AVGNumSRLpePagetoSG0AnyTime", "AVGNumSRLpePagetoSG1AnyTime"]

feat_MLP_Actions_labels = ["TimeSpentWithContentOverall", "NumberSG", "TotalTimeSettingSG", "NoteCheckingDuration", "NoteCheckingNum", "TimeINF", "numPLAN", "numJOL", "numFOK", "TimeFullView_total"]

feat_MLP_Full_labels = ["OverallLearningGoal_proptransfrom_TableOfContents", "Subgoals_proportionnum_dynamic", "Subgoals_proptransfrom_TableOfContents", "TableOfContents_fixationrate", "ImageContent_proptransfrom_ImageContent", "NoteCheckingDuration", "TimePKA", "RatioPagesRelevantToInitialSubgoal1ReadWhenSubgoal1Active", "numCE", "AVGNumSRLpePagetoSG0AnyTime"]

feat_NB_Gaze_labels = ["meanrelpathangles", "OverallLearningGoal_proptransfrom_TableOfContents", "Subgoals_proportionnum_dynamic", "Subgoals_proptransfrom_OverallLearningGoal", "Subgoals_proptransfrom_Subgoals", "LearningStrategiesPalette_proptransfrom_Agent", "Agent_proptransfrom_OverallLearningGoal", "TableOfContents_proptransfrom_TableOfContents", "TextContent_proportionnum_dynamic", "ImageContent_proptransfrom_ImageContent"]

feat_NB_Full_labels = ["meanrelpathangles", "OverallLearningGoal_proptransfrom_TableOfContents", "Subgoals_proportionnum_dynamic", "Subgoals_proptransfrom_TableOfContents", "Agent_proptransfrom_OverallLearningGoal", "TableOfContents_proptransfrom_TableOfContents", "TextContent_proportionnum_dynamic", "TimeSpentWithContentOverall", "TotalTimeWithRelTextContent", "AVGNumSRLpePagetoSG0AnyTime"]

feat_SMO_Full_labels = ["meanrelpathangles", "OverallLearningGoal_proptransfrom_TableOfContents", "Subgoals_fixationrate", "Subgoals_proportionnum_dynamic", "Subgoals_proptransfrom_TableOfContents", "TextContent_proptransfrom_TableOfContents", "ImageContent_proptransfrom_Agent", "NoteCheckingDuration", "TimeINF", "AVGNumSRLpePagetoSG0AnyTime"]

feat_SMO_Gaze_labels = ["meanrelpathangles", "stddevfixationduration", "Subgoals_proportionnum_dynamic", "Subgoals_proptransfrom_TableOfContents", "LearningStrategiesPalette_proptransfrom_Agent", "LearningStrategiesPalette_proptransfrom_TableOfContents", "TableOfContents_proptransfrom_LearningStrategiesPalette", "LearningStrategiesPalette_proptransfrom_ImageContent", "ImageContent_proptransfrom_Agent", "ImageContent_proptransfrom_Subgoals"]

first_Col_is_label = True
